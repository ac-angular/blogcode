# Blogcode

Blog de post, con control de usuarios para la publicación de los mismos.

## Conceptos a desarrollar

Angular: modules, components, ruotes, guards, interfaces, services, reactive forms

Angular Material: uso de distintos componentes
  
Firebase: authentication, database, storage, hosting

Web: login/logout, admin, profile, posts


## Tecnologías

Lenguaje: TypeScript

Framework: Angular 8

Gestor de paquetes: npm

CLI: ng, firebase

Gestor de Base de Datos: Firebase Realtime Database (NoSQL).  Los datos se almacenan en formato JSON

## Uso
Para iniciar la aplicación ejecutar el siguiente comando:

```bash
1) ng serve -o
```

## Back-End

Crear el proyecto firebase al que se pueda asociar la web.

[firebase](https://console.firebase.google.com/)

## Referencias
[Domini Code](https://www.youtube.com/playlist?list=PL_9MDdjVuFjFPCptPjhr3iuzPK0_Nrm0s)
