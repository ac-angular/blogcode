export interface PostI {
  id?: string;
  titlePost: string;
  contentPost: string;
  imagePost?: any;
  tagsPost: string;
  filePost?: string;
}
