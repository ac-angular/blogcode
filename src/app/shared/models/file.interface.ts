export interface FileI {
  nameFile: string;
  imageFile: File;
  sizeFile: string;
  typeFile: string;
}
