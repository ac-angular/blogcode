import { Injectable } from '@angular/core';
import { UserI } from '../models/user.interface';
import { FileI } from '../models/file.interface';
import { AngularFireAuth } from '@angular/fire/auth';
import { Observable } from 'rxjs';
import { AngularFireStorage } from '@angular/fire/storage';
import { finalize } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  private filePath: string;

  public userData$: Observable<firebase.User>;

  constructor(private angularFireAuth: AngularFireAuth, private storage: AngularFireStorage) {
    this.userData$ = angularFireAuth.authState;
   }

  loginByEmail(user: UserI) {
    const { email, password } = user;
    return this.angularFireAuth.auth.signInWithEmailAndPassword(email, password);
  }

  logout() {
    this.angularFireAuth.auth.signOut();
  }

  preSaveUserProfile(user: UserI, image?: FileI): void {
    if (image) {
      this.uploadImage(user, image);
    } else {
      this.saveUserProfile(user);
    }
  }

  private uploadImage(user: UserI, image: FileI): void {
    this.filePath = `images/${image.nameFile}`;
    const fileRef = this.storage.ref(this.filePath);
    const task = this.storage.upload(this.filePath, image);
    task.snapshotChanges()
    .pipe(
      finalize( () => {
        fileRef.getDownloadURL().subscribe(urlImage => {
          user.photoURL = urlImage;
          this.saveUserProfile(user);
        });
      })
    ).subscribe();
  }

  private saveUserProfile(user: UserI) {
    this.angularFireAuth.auth.currentUser.updateProfile({
      displayName: user.displayName,
      photoURL: user.photoURL
    })
    .then( () => { console.log('User updated'); })
    .catch(err => { console.log('Error', err); });
  }
}
